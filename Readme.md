# Pacemaker

Description: Assignment for design patterns module of MSc in Computing W.I.T.
- - -
## Project Setup

How do I, as a developer, start working on the project? 

Installation:

1. Copy folder to local web server and start server
2. Install node (http://nodejs.org/)
3. open command line and locate backend(node) root.
4. Run 'npm install'
5. Run 'npm start'
6. open http://localhost/Pacemaker/DesktopMinified  ( client )
7. open http://localhost/Pacemaker/SwaggerUI  ( api documentation )
8. Enjoy!

#####Bonus!
Backend published as npm module.  
use 'npm install pacemaker'  
https://www.npmjs.org/package/pacemaker

###Deployed  
http://pacemaker.davidffrench.com  ( client )  
http://api.pacemaker.davidffrench.com  ( api documentation )

- - -
## License
The MIT License

Copyright (C) 2014 David Ffrench

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.